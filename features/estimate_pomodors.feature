Feature: Estimating effort
	
	In order to better manage time and track productivity
	As a user
	I want to set expected completion time in terms of pomodors


	Scenario: Setting effort number on a task

		Given a task should take 3 sessions to complete
		When the user enters the task
		Then they should select 3 pomodors 

	Scenario: Completing pomodoros

		Given a task of 4 pomodors
		When user completes 2 sessions
		Then two of four pomodors should be crossed off that tasks list


	Scenario: Totaling pomodoros
		Given a tasklist of 2 task of 3 pomodoros each
			And focus sessions set to 25 minutes and breaks to 5
		When the user views the stats
		Then the should see a total of 6 pomodors, 12 hours
