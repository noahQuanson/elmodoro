Feature: Saving tasks

	In order to be more productive. As a user, I want to save a task list.

	Scenario: Adding a task

		Given I enter a task name "buy milk"
		When I click "Add Task"
		Then the task should be saved to the task list

	Scenario: Adding a task with no data

		Given Kevin enters no data in the task form
		When he click "Add Task"
		Then he should receive an error message

	Scenario: Setting a due date
		Given Janice has entered a task name and due date
		When she clicks "save"
		Then the the task with a name, and due date should be set
