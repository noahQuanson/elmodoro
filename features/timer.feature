Feature: Pomodoro timer

	In order to better manage time and focus, 
	I want the app to alert me when I have worked for 'x' minutes 
		and prompt me to break for 'y' minutes
	so that I can follow the pomodoro method and be more productive.

	Background: Given the user has saved 
		25 minutes as their focus limit and 
		5 minutes as their break time and
		15 minutes as their long break
		after every third focus session
	

	Scenario: Starting the timer
		Given I have selected a task
		When I click 'Start Timer'
		Then the timer should start counting down from 25 minutes

	Scenario: Focus timer ends the first or second time
		Given the focus timer is running
		When it counts down to zero
		Then it should alert the user to start the break timer

	Scenario: Short break timer starts (1st, or second time)
		Given the focus timer has ended
		When the user clicks "Start Break"
		Then it should start counting down the short break timer

	Scenario: Focus timer ends the third time
		Given the focus timer is running
		When the count reaches zero the third (6th, 9th...) time
		Then it should alert the user to start the long break timer

	Scenario: Break ends, no tasks
		Given an empty task list
		When the break timer ends
		Then it should prompt the user to add a new task


