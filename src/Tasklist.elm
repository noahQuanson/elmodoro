module Tasklist exposing (Duty, Model, add, complete, completeDuty, delete, get, init, length, list, newDuty, update)

import Dict exposing (Dict)


type alias Duty =
    { name : String
    , expectedDuration : Int
    , complete : Bool
    }


newDuty : Duty
newDuty =
    Duty "" 0 False


type Model
    = Tasklist (Dict Int Duty)


length : Model -> Int
length model =
    case model of
        Tasklist tasks ->
            Dict.size tasks


init : Model
init =
    Tasklist Dict.empty


add : Duty -> Model -> Model
add d m =
    case m of
        Tasklist tasks ->
            Dict.insert (Dict.size tasks) d tasks
                |> Tasklist


delete : Int -> Model -> Model
delete id model =
    case model of
        Tasklist tasks ->
            Dict.remove id tasks
                |> Dict.values
                |> List.indexedMap Tuple.pair
                |> Dict.fromList
                |> Tasklist


complete : Int -> Model -> Model
complete id model =
    case model of
        Tasklist tasks ->
            Dict.update id (Maybe.map completeDuty) tasks
                |> Tasklist


list : Model -> List Duty
list model =
    case model of
        Tasklist tasks ->
            Dict.values tasks


update : Int -> Duty -> Model -> Model
update id duty model =
    case model of
        Tasklist tasks ->
            Dict.update id (Maybe.map (\_ -> duty)) tasks
                |> Tasklist


get : Int -> Model -> Duty
get id model =
    case model of
        Tasklist tasks ->
            Dict.get id tasks
                |> Maybe.withDefault newDuty


completeDuty : Duty -> Duty
completeDuty d =
    { d | complete = True }
