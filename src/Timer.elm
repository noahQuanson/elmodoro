module Timer exposing (Config, Timer, breakTotal, defaultConfig, init, setSession, tick, toggle, view, viewStats)

import Element exposing (..)
import Element.Background as Background
import Element.Font as Font



-- MODEL --


type Timer
    = Timer Internals


init : Config -> Timer
init cfg =
    Timer
        { seconds = abs cfg.focusMinutes * 60
        , config = cfg
        , running = False
        , onBreak = False
        , session = 0
        }


type alias Internals =
    { seconds : Int
    , session : Int
    , running : Bool
    , onBreak : Bool
    , config : Config
    }


type alias Config =
    { focusMinutes : Int
    , shortBreakMinutes : Int
    , longBreakMinutes : Int
    , longBreakIntervals : Int
    }


pattern : Internals -> Element msg
pattern { config, session } =
    List.range 1 config.longBreakIntervals
        |> List.map
            (\n ->
                "Work "
                    ++ String.fromInt config.focusMinutes
                    ++ "m | "
                    ++ (if modBy config.longBreakIntervals n == 0 then
                            String.fromInt config.longBreakMinutes ++ "m Break | "

                        else
                            String.fromInt config.shortBreakMinutes ++ "m Break | "
                       )
            )
        |> List.map (\s -> el [] (text s))
        |> row
            [ spacing 8 ]


status : Internals -> String
status i =
    if i.onBreak && (modBy i.config.longBreakIntervals i.session == 0) then
        "Short Break"

    else if i.onBreak then
        "Long Break"

    else if i.running then
        "Focus"

    else
        ""


defaultConfig : Config
defaultConfig =
    { focusMinutes = 25
    , shortBreakMinutes = 5
    , longBreakMinutes = 15
    , longBreakIntervals = 3
    }



-- UPDATE --


tick : Timer -> Timer
tick timer =
    case timer of
        Timer internals ->
            if not internals.running then
                Timer internals

            else if internals.seconds > 0 then
                Timer { internals | seconds = internals.seconds - 1 }

            else
                Timer (completeTime internals)


completeTime : Internals -> Internals
completeTime internals =
    if internals.onBreak then
        { internals
            | seconds = internals.config.focusMinutes * 60 |> abs
            , session = internals.session + 1
            , onBreak = False
            , running = False
        }

    else if modBy internals.config.longBreakIntervals (internals.session + 1) == 0 then
        { internals
            | seconds = internals.config.longBreakMinutes * 60 |> abs
            , running = False
            , onBreak = True
        }

    else
        { internals
            | seconds = internals.config.shortBreakMinutes * 60 |> abs
            , running = False
            , onBreak = True
        }


toggle : Timer -> Timer
toggle timer =
    case timer of
        Timer internals ->
            if internals.onBreak then
                Timer { internals | running = not internals.running }

            else
                Timer
                    { internals
                        | running = not internals.running
                        , seconds = internals.config.focusMinutes * 60 |> abs
                    }


setSession : Int -> Timer -> Timer
setSession s timer =
    case timer of
        Timer internals ->
            Timer { internals | session = s }



-- VIEW --


view : Timer -> Element msg
view timer =
    case timer of
        Timer internals ->
            column
                [ width fill
                , height fill
                ]
                [ column
                    [ height (fillPortion 7), width fill, explain Debug.todo ]
                    [ viewTime internals.seconds
                        |> el
                            [ Font.size 64
                            , centerX
                            , centerY
                            , Font.center
                            , below <| el [ Font.size 24, centerX, Font.center ] (text (status internals))
                            ]
                    , pattern internals
                    ]
                , viewStats timer
                ]


viewStats : Timer -> Element msg
viewStats timer =
    case timer of
        Timer i ->
            row
                [ spaceEvenly
                , width fill
                , padding 8
                , Background.color (rgb255 165 175 195)
                ]
                [ paragraph
                    [ spacing 8, width fill ]
                    [ text "Session #"
                    , el [] (String.fromInt i.session |> text)
                    ]
                , column
                    []
                    [ row
                        [ spacing 8, width fill ]
                        [ text "Focus Time:"
                        , viewTime (i.config.focusMinutes * 60 * i.session)
                        ]
                    , row
                        [ spacing 8, width fill ]
                        [ text "Break Time:"
                        , viewTime (shortBreak_ i + longBreak_ i) |> el [ alignRight ]
                        ]
                    , row
                        [ spacing 8, width fill ]
                        [ text "Total:"
                        , viewTime (shortBreak_ i + longBreak_ i + (i.config.focusMinutes * 60 * i.session))
                            |> el
                                [ alignRight
                                ]
                        ]
                    ]
                ]


viewTime : Int -> Element msg
viewTime seconds =
    row
        [ spaceEvenly ]
        [ viewHours seconds
        , if seconds >= 3600 then
            ":" |> text

          else
            none
        , viewMinutes (modBy 3600 seconds)
        , ":" |> text
        , viewSeconds (modBy 60 seconds)
        ]


viewMinutes : Int -> Element msg
viewMinutes seconds =
    seconds
        // 60
        |> String.fromInt
        |> String.padLeft 2 '0'
        |> text


viewSeconds : Int -> Element msg
viewSeconds seconds =
    modBy 60 seconds
        |> String.fromInt
        |> String.padLeft 2 '0'
        |> text


viewHours : Int -> Element msg
viewHours seconds =
    if seconds // 3600 == 0 then
        Element.none

    else
        seconds
            // 3600
            |> String.fromInt
            |> String.padLeft 2 '0'
            |> text


shortBreakTotal : Timer -> Int
shortBreakTotal timer =
    case timer of
        Timer i ->
            shortBreak_ i


longBreakTotal : Timer -> Int
longBreakTotal timer =
    case timer of
        Timer i ->
            longBreak_ i


longBreak_ : Internals -> Int
longBreak_ { session, config } =
    60 * (session // config.longBreakIntervals) * abs config.longBreakMinutes


shortBreak_ : Internals -> Int
shortBreak_ { session, config } =
    60 * (session - (session // config.longBreakIntervals)) * abs config.shortBreakMinutes


breakTotal : Timer -> Int
breakTotal timer =
    shortBreakTotal timer
        + longBreakTotal timer
