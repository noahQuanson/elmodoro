module Main exposing (main)

import Browser
import Browser.Dom as Dom
import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Events as Events
import Element.Font as Font
import Element.Input as Input
import Html exposing (Html)
import Html.Attributes as Attr
import Task
import Tasklist exposing (Duty)
import Time
import Timer exposing (Timer)


main : Program () Model Msg
main =
    Browser.element
        { init = \_ -> ( init, Cmd.none )
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


type alias Model =
    { tasklist : Tasklist.Model
    , timer : Timer
    , editIndex : Maybe Int
    , duty : Duty
    }


init : Model
init =
    { tasklist = Tasklist.init
    , timer = Timer.init Timer.defaultConfig
    , editIndex = Nothing
    , duty = Tasklist.newDuty
    }


view : Model -> Html Msg
view model =
    layout
        []
    <|
        column
            [ width fill
            , height fill
            ]
            [ row
                [ width fill
                , height (fillPortion 3)
                ]
                [ Timer.view model.timer
                    |> el [ width (fillPortion 4), height fill, Events.onClick Toggle, pointer ]
                , column
                    [ width fill
                    , height fill
                    ]
                    [ viewTasklist model
                    , Input.button
                        [ Background.color (rgb255 15 155 77)
                        , Font.color (rgb255 225 225 240)
                        , Font.center
                        , padding 8
                        , width fill
                        ]
                        { onPress = Just AddDuty
                        , label = text "Add Task"
                        }
                    ]
                ]
            ]


viewEditTask : Duty -> Element Msg
viewEditTask duty =
    row
        [ width fill
        , padding 16
        , spacing 8
        , Background.color (rgb255 100 100 105)
        , centerY
        , below <| el [ Events.onClick ClickedCancel, Font.size 8, alignRight, padding 8 ] (text "cancel")
        ]
        [ row
            [ width fill
            , spacing 16
            ]
            [ Input.text
                [ width (fillPortion 5), focused [ Border.glow (Element.rgb 0.2 0.4 0.8) 2 ], htmlAttribute <| Attr.id "description" ]
                { onChange = \n -> EditDuty { duty | name = n }
                , text = duty.name
                , placeholder = Nothing
                , label = Input.labelHidden "Task Description"
                }
            , Input.button
                [ Border.width 2
                , Font.color (rgb255 255 255 255)
                , Font.center
                , width (fillPortion 2)
                , if String.length duty.name > 2 then
                    Background.color (rgb255 50 200 75)

                  else
                    Background.color (rgb255 75 75 75)
                , paddingXY 4 8
                , alignBottom
                ]
                { onPress =
                    if String.length duty.name > 2 then
                        Just ClickedSave

                    else
                        Nothing
                , label = text "Save"
                }
            ]
        ]


viewTasklist : Model -> Element Msg
viewTasklist { tasklist, editIndex, duty } =
    let
        viewTask : Int -> Duty -> Element Msg
        viewTask id d =
            row
                [ width fill
                , spacing 16
                , padding 16
                , onLeft (el [ Events.onClick (ClickedDelete id), Background.color (rgb255 100 17 7), Font.color (rgb255 225 225 225) ] (text "x"))
                ]
                [ el [ centerX ] (text (String.fromInt (id + 1) ++ "."))
                , paragraph [ width (fillPortion 3) ] [ text d.name ]
                , el [ Events.onClick (ClickedEdit id), Font.size 8 ] (text "edit")
                ]

        editOrView : Int -> Duty -> Element Msg
        editOrView id d =
            case editIndex of
                Just id_ ->
                    if id == id_ then
                        viewEditTask duty

                    else
                        viewTask id d

                Nothing ->
                    viewTask id d
    in
    Tasklist.list tasklist
        |> List.indexedMap editOrView
        |> column
            [ width fill
            ]
        |> List.singleton
        |> column [ height fill, width fill, Background.color (rgb255 200 200 210) ]



-- UPDATE --


type Msg
    = EditDuty Duty
    | ClickedEdit Int
    | ClickedDelete Int
    | ClickedSave
    | ClickedCancel
    | AddDuty
    | Tick Time.Posix
    | Toggle
    | Focus String
    | Focused (Result Dom.Error ())


focusElement : String -> Cmd Msg
focusElement htmlId =
    Task.attempt Focused (Dom.focus htmlId)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Focus htmlId ->
            ( model, focusElement htmlId )

        Focused _ ->
            ( model, Cmd.none )

        ClickedSave ->
            case model.editIndex of
                Just id ->
                    if String.length model.duty.name > 2 then
                        ( { model | tasklist = Tasklist.update id model.duty model.tasklist, editIndex = Nothing, duty = Tasklist.newDuty }, Cmd.none )

                    else
                        ( model, Cmd.none )

                Nothing ->
                    ( model, Cmd.none )

        EditDuty duty ->
            ( { model | duty = duty }, focusElement "description" )

        ClickedCancel ->
            case model.editIndex of
                Just id ->
                    if Tasklist.get id model.tasklist |> .name |> String.isEmpty then
                        ( { model | editIndex = Nothing, tasklist = Tasklist.delete id model.tasklist }, Cmd.none )

                    else
                        ( { model | editIndex = Nothing }, Cmd.none )

                Nothing ->
                    ( { model | editIndex = Nothing }, Cmd.none )

        ClickedEdit id ->
            ( { model | editIndex = Just id, duty = Tasklist.get id model.tasklist }, focusElement "description" )

        ClickedDelete id ->
            ( { model | editIndex = Nothing, duty = Tasklist.newDuty, tasklist = Tasklist.delete id model.tasklist }, Cmd.none )

        AddDuty ->
            case model.editIndex of
                Just id ->
                    if String.isEmpty <| .name <| Tasklist.get id model.tasklist then
                        ( model, Cmd.none )

                    else
                        ( { model
                            | tasklist = Tasklist.add Tasklist.newDuty model.tasklist
                            , editIndex = Just (Tasklist.length model.tasklist)
                            , duty = Tasklist.newDuty
                          }
                        , focusElement "description"
                        )

                Nothing ->
                    ( { model
                        | tasklist = Tasklist.add Tasklist.newDuty model.tasklist
                        , editIndex = Just (Tasklist.length model.tasklist)
                        , duty = Tasklist.newDuty
                      }
                    , focusElement "description"
                    )

        Tick _ ->
            ( { model | timer = Timer.tick model.timer }, Cmd.none )

        Toggle ->
            ( { model | timer = Timer.toggle model.timer }, Cmd.none )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions _ =
    Time.every 0.001 Tick
