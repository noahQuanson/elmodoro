module TestTasklist exposing (..)

import Expect
import Test exposing (..)
import Tasklist


suite : Test
suite =
    describe "Tasklist manage tasks"
        [ test "It can add tasks" <|
            \_-> Tasklist.init
                |> Tasklist.add (newDuty "learn elm" 1) 
                |> Tasklist.add (newDuty "learn go" 2) 
                |> Tasklist.length 
                |> Expect.equal 2
        , test "Given a task list of 3, when I delete the third task, then the first two remain" <|
            \_-> Tasklist.init
                |> Tasklist.add (newDuty "learn elm" 1) 
                |> Tasklist.add (newDuty "learn go" 2) 
                |> Tasklist.add (newDuty "learn rust" 5) 
                |> Tasklist.delete 2
                |> Tasklist.list
                |> Expect.equal [newDuty "learn elm" 1, newDuty "learn go" 2]
        , test "Given a task list of 3, when I delete the 4th or higher (or 0 or less), then the list remains the same" <|
            \_-> Tasklist.init
                |> Tasklist.add (newDuty "learn elm" 1) 
                |> Tasklist.add (newDuty "learn go" 2) 
                |> Tasklist.delete -3
                |> Tasklist.list
                |> Expect.equal [newDuty "learn elm" 1, newDuty "learn go" 2]
        , test "Given a task list of 3, when I complete the second task, the the second task should be complete" <|
            \_-> Tasklist.init
                |> Tasklist.add (newDuty "learn elm" 1) 
                |> Tasklist.add (newDuty "learn go" 2) 
                |> Tasklist.add (newDuty "learn rust" 5) 
                |> Tasklist.complete 1
                |> Tasklist.list
                |> Expect.equal 
                    [ newDuty "learn elm" 1
                    , newDuty "learn go" 2 |> Tasklist.completeDuty
                    , newDuty "learn rust" 5
                    ]
        ]


newDuty : String -> Int -> Tasklist.Duty
newDuty  name duration =
    let
        d = Tasklist.newDuty
    in
    { d | name = name, expectedDuration = abs duration}