module TestTimer exposing (..)

import Expect exposing (Expectation)
import Test exposing (..)
import Timer


suite : Test
suite =
    describe "Break time"
        [ test
            """
            Given a config of 25 minute focus sessions, 5 minute short breaks, 15m long breaks, with long breaks every 3rd session
            When it is on the 5th session
            Then the total time break total should be 50 minutes
            """
          <|
            \_ ->
                Timer.init Timer.defaultConfig
                    |> Timer.setSession 6
                    |> Timer.breakTotal
                    |> Expect.equal ((4 * 5) * 60 + (2 * 15) * 60)
        ]
